package com.simple.accessdatabase.databaseaccess.controller;

import com.simple.accessdatabase.databaseaccess.model.Users;
import com.simple.accessdatabase.databaseaccess.repository.UsersRepository;
import javassist.tools.web.BadHttpRequest;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;
import java.util.Optional;

//rest lewat controller, karena mengkonfigurasi rest
@RestController
@RequestMapping (path = "/users")

public class UserController {

    @Autowired //untuk panggil filenya
    private UsersRepository repository;

    @GetMapping // untuk ngeread data
    //metadata = pengenalan
    public Iterable<Users> getAll() // liat data yang dibuat semuanya
    {
        return repository.findAll();
    }


    @GetMapping (path = "/{username}")
    public Optional<Users>find (@PathVariable(value= "username")String username) {
        return  repository.findById(username);
    }

    @PostMapping (consumes = "application/json")// post data dari aplikasi json
    public Users create(@RequestBody Users users){
        return repository.save(users);
    }

    @DeleteMapping (path ="/{username}")
    public void delete (@PathVariable("username") String username){
        repository.deleteById(username);
    }

    @PutMapping(path = "/{username}")
    public Users update(@PathVariable("username") String username, @RequestBody Users users) throws BadHttpRequest{
        if(repository.existsById(username))
        {
            users.setUsername(username);
            return repository.save(users);
        }

            else {
                throw new BadHttpRequest();
        }
        }
    }

