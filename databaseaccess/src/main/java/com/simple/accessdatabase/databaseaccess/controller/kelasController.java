package com.simple.accessdatabase.databaseaccess.controller;


import com.simple.accessdatabase.databaseaccess.model.Kelas;
import com.simple.accessdatabase.databaseaccess.repository.KelasRepository;
import javassist.tools.web.BadHttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

import java.util.Optional;

@RestController
@RequestMapping(path = "/kelas")
public class kelasController {

    @Autowired
    private KelasRepository repository;

    @GetMapping
    public Iterable<Kelas> getAll(){
    return  repository.findAll();
}
    @GetMapping(path = "/{namakelas}")
    public Optional<Kelas> find(@PathVariable(value="namakelas") String namakelas){
        return repository.findById(namakelas);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(consumes = "application/json")
    public Kelas create(@RequestBody Kelas kelas){
        return repository.save(kelas);
    }

    @DeleteMapping(path = "/[namakelas]")
    public void delete(@PathVariable(value = "namakelas") String namakelas){
        repository.deleteById(namakelas);
    }

   @PutMapping (path = "/{namakelas}")
    public Kelas uodate(@PathVariable(value ="namakelas") String namakelas, @RequestBody Kelas kelas) throws BadHttpRequest{

       if(repository.existsById(namakelas))
        {
            kelas.setNamakelas(namakelas);
            return repository.save(kelas);
        }

            else {
                throw new BadHttpRequest();
        }
    }




}
