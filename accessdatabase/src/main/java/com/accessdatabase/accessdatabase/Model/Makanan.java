package com.accessdatabase.accessdatabase.Model;


import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table (name="tb_makanan")
public class Makanan implements Serializable {

    @Id
    @Column(name="idMakanan",length=150)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid", strategy="uuid")
    private String idMakanan;

    @Column (name="namaMakanan", length = 40)
    private String namaMakanan;

    @Column (nama="hargaMakanan")
    private BigDecimal hargaMakanan;

 @JsonFormat(pattern ="dd/MM/yyyy", shape = JsonFormat.Shape.STRING)
    @Column(name="tanggalKadaluarsa")
    @Temporal(TemporalType.DATE)
    private Date tanggalKadaluarsa;

    public String getIdMakanan() {
        return idMakanan;
    }

    public void setIdMakanan(String idMakanan) {
        this.idMakanan = idMakanan;
    }

    public String getNamaMakanan() {
        return namaMakanan;
    }

    public void setNamaMakanan(String namaMakanan) {
        this.namaMakanan = namaMakanan;
    }

    public BigDecimal getHargaMakanan() {
        return hargaMakanan;
    }

    public void setHargaMakanan(BigDecimal hargaMakanan) {
        this.hargaMakanan = hargaMakanan;
    }

    public Date getTanggalKadaluarsa() {
        return tanggalKadaluarsa;
    }

    public void setTanggalKadaluarsa(Date tanggalKadaluarsa) {
        this.tanggalKadaluarsa = tanggalKadaluarsa;
    }
}
