package com.accessdatabase.accessdatabase.Repository;

import com.accessdatabase.accessdatabase.Model.Makanan;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service

@Transactional(ReadOnly=true)
public class MakananServiceImplementasi implements MakananService {

    @Autowired
    private MakananRepository makananRepository;

    @Transactional
    @Override
    public void save(Makanan makanan){
        makananRepository.save(makanan);
    }

    @Transactional
    @Override
    public void update(Makanan makanan)
    {
        makananRepository.update(makanan);
    }

    2Transactional
    @Override
    public void delete(Makanan makanan)
    {
        makananRepository.delete(makanan);
    }
}
